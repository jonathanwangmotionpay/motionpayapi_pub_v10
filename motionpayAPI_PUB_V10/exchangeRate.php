<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Api.php";
require_once "./lib/Log.php";

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);

header("Content-Type:text/html;charset=utf-8");

$payChannel="A";
if(isset($_GET['payChannel'])) {
    $payChannel = $_GET['payChannel'];
}
MotionPayConfig::setMerchantCurrencyType('CAD');
$input = new MotionPayOrder();
$input->setPayChannel($payChannel);
$result = MotionPayApi::exchangeRate($input);
$content = "";
if ($result['code'] == '0') {
    $content = $result['content'];
}
else {
    $message = $result['message'];
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>

</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span >Sample Payment Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>     
      <span style="float:right;"><a href="index.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Order Page</font></a></span> 
    </div>
</div>
  <div class="bank_list accounts_pay" style="display:block;">
  <form id="exchangeRate" action="exchangeRate.php" method="get" >
       
    <?php
    if($payChannel == 'A') {
        echo "<h6><label>Currency Rate for AliPay</label></h6>";
    
        echo "<table><tr><td>alipay_ecomm_rate   &nbsp;&nbsp;</td><td>" . $content['alipay_ecomm_rate'] . "</td></tr>\n";
        echo "<tr><td>alipay_retail_rate              </td><td>" . $content['alipay_retail_rate'] . "</td></tr>\n";
        echo "<tr><td>currency_type                   </td><td>" . $content['currency_type'] . "</td></tr> </table>\n";
    }
    else if($payChannel == 'W') {
        echo "<h6><label>Currency Rate for WeChat</label></h6>";
        
        echo "<table><tr><td>wechatpay_rate &nbsp;&nbsp;   </td><td>" . $content['wechatpay_rate'] . "</td></tr>\n";
        echo "<tr><td>currency_type                 </td><td>" . $content['currency_type'] . "</td></tr>  </table>\n";
    }
    ?>
    
    <br/><br/>
    
    <input type="radio" id="payChannel" name="payChannel" value="A" /><label for="A">AliPay</label>
    <input type="radio" id="payChannel" name="payChannel" value="W" /><label for="W">WeChat</label>
    <input type="submit" value="Query"/>
   
 
  </form>   
  </div>
</div>

<script>
function getBaseURL() {
    var loc = window.location;
    var baseURL = loc.protocol + "//" + loc.hostname;
    if (typeof loc.port !== "undefined" && loc.port !== "") baseURL += ":" + loc.port;
    // strip leading /
    var pathname = loc.pathname;
    if (pathname.length > 0 && pathname.substr(0,1) === "/") pathname = pathname.substr(1, pathname.length - 1);
    var pathParts = pathname.split("/");
    if (pathParts.length > 0) {
        for (var i = 0; i < pathParts.length; i++) {
            if (pathParts[i] !== "") baseURL += "/" + pathParts[i];
        }
    }
    // alert("url is:" + baseURL);
    return baseURL;
}
function setCurrentURL() {	
	var currentURL = document.getElementById("currentURL");
	if(currentURL != null) {
		// alert("not null!");
		currentURL.value = getBaseURL()  + "/";
		// alert(currentURL.value);
	}
}
setCurrentURL();
</script>	
</body>

</html>


