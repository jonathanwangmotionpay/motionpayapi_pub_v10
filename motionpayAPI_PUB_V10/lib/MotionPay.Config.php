<?php

/**
 * All Configuration information
 * @author Jonathan
 */

/**
 * 配置账号信息
 * @author Jonathan
 */
class MotionPayConfig
{
    //=======Web Server inforamtion=====================================
    //=======【基本信息设置】=====================================
    /**
     * Server's IP Address
     * 服务器IP地址
     */
    const SPBILL_CREATE_IP = '192.168.1.1';
    /**
     * log file folder name. We will use log folder in the package. We won't use /tmp. Please grant read and write permission to this log folder.
     * 日志文件存放目录 程序初始化时 会更改日志目录为 当前路径下的 log 目录，不会使用 /tmp. 请对该log目录设置apache用户的读写权限。
     */
    protected static $LOG_FOLDER = '/tmp/';    
    /**
     * call back function log filename
     * 异步通知的日志文件名称
     */
    const CALLBACK_LOG_FILENAME = '';
    /**
     * the deployed URL for this demo SDK, please change it to your own URL to get the call back notification
     * 该 demo SDK部署后的 URL, 
     */    
    protected static $DEMO_SERVER_URL = 'https://php.motionpay.org/motionpayAPI_PUB/';
    /**
     * call back function filename
     * 异步通知程序文件名称
     */
    const CALLBACK_URL = 'callback.php';
    /**
     * The page for H5 payment is done.
     * H5 支付完成后的跳转页面
     */
    const WAP_URL = 'paymentDone.php';
    /**
     * The page for payment notification.
     * 检查订单是否已经支付的页面
     */
    const PAYMENT_NOTIFY_URL = 'paymentNotify.php';
    
    /**
     * The page for payment complete.
     * 支付成功后商户跳转页面
     */
    const PAYMENT_COMPLETE_URL = 'paymentDone.php';
    /**
     * The page for payment cancel.
     * 取消支付时，返回的商户指定页面
     */
    const PAYMENT_CANCEL_URL = 'hostedPage.php';
    
    
    
    //=======MotionPay Account Information=====================================
    //=======MotionPay API 所使用的测试账号信息=======================================
    /**
     * TODO: Update the live account information when you go live
     * We have three types of merchant. ONLINE, OFFLINE and H5.
     * Every type of merchant has its own MerchantID (mid), API_ID and API_PASSWORD
     * The account information here is for testing. When you get your own live MerchantID, API_ID and API_PASSWORD.
     * Please keep it secure. Once the API_ID and API_PASSWORD is not secure any more. Please contact us immediately to get a new one.
     * 
     * MERCHANT_ID: the MERCHANT ID (required)
     * API_ID:      the matching API Id (required)
     * API_PASSWORD: the matching API password (required) Live account API password is top secure.
     */
    /**
     * TODO: 上线时修改生产服务器的配置为您自己申请的商户信息
     * 我们有三种类型的商户，线上(ONLINE)， 线下(OFFLINE)，和 H5.
     * 每种商户都对应一个 商户号，接口代码，接口密码。
     * 这里的账号信息是用于测试的。当您获得了正式的商户号，接口ID和密码时候，您可以替换掉这里的账户信息。
     * 您的正式的商户号，接口ID和密码是需要严格保密的信息，一旦您的APP_ID或是APP_PASSWORD泄密，请立刻联系我们以获取新的ID和密码。
     *
     * MERCHANT_ID: 绑定支付的MERCHANT_ID（必须配置）
     * API_ID: 系统为商户分配的API_ID（必须配置）
     * API_PASSWORD: 系统为商户分配的API密码（必须配置）
     *
     */

    /**
       from version 1.9 (August 2020). one mid can handle online, offline and H5.
       so we can use any of these merchant type. The mid is only related by Merchant Currency Type now.

     * We have three type of merchant. You should be one or two of them. ONLINE for PC web browser. OFFLINE for local stores. H5 for smart phone orders.
     * @var string constant values for the Merchant type
     */
    /**
     * 我们有三种商户类型，ONLINE是个人电脑浏览器生产QR码，然后用手机扫描的商户类型，OFFLINE是本地商户的线下交易商户类型，H5是用智能手机直接支付的商户类型。您可以使用其中的一种或多种。
     * @var string 固定常量，表示商户类型
     */    
    const ONLINE_MERCHANT = 'ONLINE';
    const OFFLINE_MERCHANT = 'OFFLINE';
    const H5_MERCHANT = 'H5';
    
    /**
     * The merchant currecny is the bank account currency type for merchant. 
     * The merchant can have the price in other currency.
     * 这里的商户货币类型，指的是商户的银行货币类型，也就是结算货币类型，商户的商品价格可以是其他货币类型。
     */
    const USD_MERCHANT = 'USD';
    const CAD_MERCHANT = 'CAD';
    const CNY_MERCHANT = 'CNY';
    
    /**
     * The flag to use the live API server or the test API server. Please start with test API server. When the integration testing is done, you can set it to true to use the live API server.
     * @var boolean true to use live API server, false to use the test API server.
     */
    /**
     * 用于设置使用测试服务器还是生产服务器的标志位，请在集成测试时使用测试服务器，集成完成后使用生产服务器。
     * @var boolean true使用生产服务器， false使用测试服务器。
     */
    protected static $USING_LIVE_APISERVER = false;
    
    protected static $USING_MERCHANT_CURRECNY_TYPE = self::CAD_MERCHANT;
    
    /**
     * These Ids and passwords are for test server CAD price.
     * 这里的信息是测试服务器上的测试账号信息
     */
    protected static $MERCHANT_IDS_TEST_CAD = array(self::ONLINE_MERCHANT=>"100105100000011",self::OFFLINE_MERCHANT=>"100105100000011",self::H5_MERCHANT=>"100105100000011");
    protected static $API_IDS_TEST_CAD = array(self::ONLINE_MERCHANT=>"5005642017006",self::OFFLINE_MERCHANT=>"5005642017006",self::H5_MERCHANT=>"5005642017006");
    protected static $API_PASSWORDS_TEST_CAD = array(self::ONLINE_MERCHANT=>"2ce9f51261e21ba6a087ee239160f0b1",self::OFFLINE_MERCHANT=>"2ce9f51261e21ba6a087ee239160f0b1",self::H5_MERCHANT=>"2ce9f51261e21ba6a087ee239160f0b1");
    
    /**
     * These Ids and passwords are for test server CNY price.
     * 这里的信息是测试服务器上的测试账号信息
     */
    protected static $MERCHANT_IDS_TEST_CNY = array(self::ONLINE_MERCHANT=>"100122220000003",self::OFFLINE_MERCHANT=>"100122220000003",self::H5_MERCHANT=>"100122220000003");
    protected static $API_IDS_TEST_CNY = array(self::ONLINE_MERCHANT=>"5005642020001",self::OFFLINE_MERCHANT=>"5005642020001",self::H5_MERCHANT=>"5005642020001");
    protected static $API_PASSWORDS_TEST_CNY = array(self::ONLINE_MERCHANT=>"cea2666c382fb3c8883d51275fe32826",self::OFFLINE_MERCHANT=>"cea2666c382fb3c8883d51275fe32826",self::H5_MERCHANT=>"cea2666c382fb3c8883d51275fe32826");

    /**
     * These Ids and passwords are for test server USD price.
     * 这里的信息是测试服务器上的测试账号信息
     */
    protected static $MERCHANT_IDS_TEST_USD = array(self::ONLINE_MERCHANT=>"100122220000004",self::OFFLINE_MERCHANT=>"100122220000004",self::H5_MERCHANT=>"100122220000004");
    protected static $API_IDS_TEST_USD = array(self::ONLINE_MERCHANT=>"5005642019002",self::OFFLINE_MERCHANT=>"5005642019002",self::H5_MERCHANT=>"5005642019002");
    protected static $API_PASSWORDS_TEST_USD = array(self::ONLINE_MERCHANT=>"d228f2081e4c739d877affc448ac7d71",self::OFFLINE_MERCHANT=>"d228f2081e4c739d877affc448ac7d71",self::H5_MERCHANT=>"d228f2081e4c739d877affc448ac7d71");
    

    /**
     * These Ids and passwords are for test server current using.
     * 这里的信息是测试服务器上的测试账号信息
     */
    protected static $MERCHANT_IDS_TEST = array(self::ONLINE_MERCHANT=>"100122220000004",self::OFFLINE_MERCHANT=>"100122220000004",self::H5_MERCHANT=>"100122220000004");
    protected static $API_IDS_TEST = array(self::ONLINE_MERCHANT=>"5005642019002",self::OFFLINE_MERCHANT=>"5005642019002",self::H5_MERCHANT=>"5005642019002");
    protected static $API_PASSWORDS_TEST = array(self::ONLINE_MERCHANT=>"d228f2081e4c739d877affc448ac7d71",self::OFFLINE_MERCHANT=>"d228f2081e4c739d877affc448ac7d71",self::H5_MERCHANT=>"d228f2081e4c739d877affc448ac7d71");
    
    /**
     * These Ids and passwords are for the live server. After you got your own live account information from motion pay, 
     * please replace the according to ids and password with the information you got. Please update all three types 
     * ONLINE, OFFLINE, and H5. This information for all these three types could be the same.
     * 这里的信息是生产服务器上的生产账号信息。您集成完毕，系统上线时需要更换成您从motion pay 申请时获得的生产服务器上的您自己商户的 专属信息。请同时更换这里的三种交易类型， 
     * ONLINE, OFFLINE, H5. 对于这三种交易类型，您可以使用同样的信息。
      */
    protected static $MERCHANT_IDS_LIVE = array(self::ONLINE_MERCHANT=>"ONLINE_MID_To_Be_Replaced_Before_Live",self::OFFLINE_MERCHANT=>"OFFLINE_MID_To_Be_Replaced_Before_Live",self::H5_MERCHANT=>"H5_MID_To_Be_Replaced_Before_Live");
    protected static $API_IDS_LIVE = array(self::ONLINE_MERCHANT=>"ONLINE_APPID_To_Be_Replaced_Before_Live",self::OFFLINE_MERCHANT=>"OFFLINE_APPID_To_Be_Replaced_Before_Live",self::H5_MERCHANT=>"H5_APPID_To_Be_Replaced_Before_Live");
    protected static $API_PASSWORDS_LIVE = array(self::ONLINE_MERCHANT=>"ONLINE_PASSWORD_To_Be_Replaced_Before_Live",self::OFFLINE_MERCHANT=>"OFFLINE_APPPASSWORD_To_Be_Replaced_Before_Live",self::H5_MERCHANT=>"H5_PASSWORDID_To_Be_Replaced_Before_Live");
    

    /**
     * MotionPay test API Server URL
     * MotionPay 测试服务器的接口地址
     * 
     */
    protected static $API_HOST_URL_TEST = 'https://api.motionpay.org/';
    
    /**
     * MotionPay production API Server URL
     * MotionPay 生产服务器的接口地址
     *
     */
    protected static $API_HOST_URL_LIVE = 'https://api.motionpaydev.com/';
	/**
	 * scan mobile
	 * 手机被扫
	 */
    const ORDER_PAY =  'onlinePayment/v1_1/pay/orderPay';
	/**
	 * mobile scan
	 * 手机主扫
	 */
    const PRE_PAY = 'onlinePayment/v1_1/pay/prePay';	
	/**
	 * H5 payment
	 * H5 手机直接支付
	 */
    const H5_PAY = 'onlinePayment/v1_1/pay/wapPay';
	/**
	 * Get H5 Pay URL
	 * 获取H5手机直接支付的网址
	 */
    const GET_PAY_URL = 'onlinePayment/v1_1/pay/getPayUrl';
	
	/**
	 * query order
	 * 查询已提交的订单的状态
	 */
    const ORDER_QUERY = 'onlinePayment/v1_1/pay/orderQuery';
	/**
	 * refund order
	 * 对已支付的订单进行退款
	 */
    const ORDER_REVOKE = 'onlinePayment/v1_1/pay/revoke';
    /**
     * get exchange rate
     * 查询当前汇率
     */
    const ORDER_GETRATE = 'onlinePayment/v1_1/pay/getRate';
    /**
     * create PC WEB Redirect Order
     * 创建电脑浏览器跳转订单
     */
    const ORDER_WEBPAY = 'onlinePayment/v1_1/pay/webPay';
    /**
     * create HOSTED Order
     * 创建HOSTED页面订单
     */
    const HOSTED_PAYMENT = 'onlinePayment/v1_1/pay/hostedPayment';

    
    //=======【curl proxy setting】============================
    //=======【curl代理设置】=====================================
    /**
     * TODO：set the proxy information here. If you don't have a proxy just leave it as 0.0.0.0 and 0
     * default value: CURL_PROXY_HOST=0.0.0.0, CURL_PROXY_PORT=0 We won't use proxy for the default value
     */
    /**
     * TODO：这里设置代理机器，只有需要代理的时候才设置，不需要代理，请设置为0.0.0.0和0
     * 默认CURL_PROXY_HOST=0.0.0.0和CURL_PROXY_PORT=0，此时不开启代理（如有需要才设置）
     */
    const CURL_PROXY_HOST = '0.0.0.0';  //'192.168.1.1';
    const CURL_PROXY_PORT = 0;  //8080;
    
    /**
     * Set the Merchant Id for Testing
     * 设置测试用的账户号，根据缺省的货币类型
     * @param string $currencyType
     */
    public static function setMerchantCurrencyType($currencyType) {
        self::$USING_MERCHANT_CURRECNY_TYPE = $currencyType;
        if($currencyType == self::CAD_MERCHANT) {
            self::$MERCHANT_IDS_TEST = self::$MERCHANT_IDS_TEST_CAD;
            self::$API_IDS_TEST = self::$API_IDS_TEST_CAD;
            self::$API_PASSWORDS_TEST = self::$API_PASSWORDS_TEST_CAD;
        }
        else if($currencyType == self::USD_MERCHANT) {
            self::$MERCHANT_IDS_TEST = self::$MERCHANT_IDS_TEST_USD;
            self::$API_IDS_TEST = self::$API_IDS_TEST_USD;
            self::$API_PASSWORDS_TEST = self::$API_PASSWORDS_TEST_USD;
        }
        else if($currencyType == self::CNY_MERCHANT) {
            self::$MERCHANT_IDS_TEST = self::$MERCHANT_IDS_TEST_CNY;
            self::$API_IDS_TEST = self::$API_IDS_TEST_CNY;
            self::$API_PASSWORDS_TEST = self::$API_PASSWORDS_TEST_CNY;
        }        
    }
    
    public static function setMerchantCurrencyTypeByMid($mid) {
        if (MotionPayConfig::getMidByTwoTypes(self::CAD_MERCHANT, self::ONLINE_MERCHANT) == $mid 
            || MotionPayConfig::getMidByTwoTypes(self::CAD_MERCHANT, self::OFFLINE_MERCHANT) == $mid 
            || MotionPayConfig::getMidByTwoTypes(self::CAD_MERCHANT, self::H5_MERCHANT) == $mid) {
            self::$USING_MERCHANT_CURRECNY_TYPE = self::CAD_MERCHANT;
            // echo "matching CAD";
        }
        else if (MotionPayConfig::getMidByTwoTypes(self::USD_MERCHANT, self::ONLINE_MERCHANT) == $mid
            || MotionPayConfig::getMidByTwoTypes(self::USD_MERCHANT, self::OFFLINE_MERCHANT) == $mid
            || MotionPayConfig::getMidByTwoTypes(self::USD_MERCHANT, self::H5_MERCHANT) == $mid) {
            self::$USING_MERCHANT_CURRECNY_TYPE = self::USD_MERCHANT;
            // echo "matching USD";
        }
        else if (MotionPayConfig::getMidByTwoTypes(self::CNY_MERCHANT, self::ONLINE_MERCHANT) == $mid
            || MotionPayConfig::getMidByTwoTypes(self::CNY_MERCHANT, self::OFFLINE_MERCHANT) == $mid
            || MotionPayConfig::getMidByTwoTypes(self::CNY_MERCHANT, self::H5_MERCHANT) == $mid) {
            self::$USING_MERCHANT_CURRECNY_TYPE = self::CNY_MERCHANT;
            // echo "matching CNY";
        }
    }
    
    /*
     * Get the mid by Merchant CurrencyType and Merchant Type
     * 获得  mid 通过商户用的结算货币类型与商户类型
     * @param string $currencyType Merchant CurrencyType
     * @param string $merchantTYpe Merchant Type Online Offline or H5
     * @return string $mid Merchant ID
     */    
    public static function getMidByTwoTypes($currencyType, $merchantType) {
        if($currencyType == self::CAD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$MERCHANT_IDS_TEST_CAD[$merchantType];
            else
                return self::$MERCHANT_IDS_LIVE[$merchantType];
        }
        else if($currencyType == self::USD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$MERCHANT_IDS_TEST_USD[$merchantType];
            else
                return self::$MERCHANT_IDS_LIVE[$merchantType];
        }
        else if($currencyType == self::CNY_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$MERCHANT_IDS_TEST_CNY[$merchantType];
            else
                return self::$MERCHANT_IDS_LIVE[$merchantType];
        }     
        return "";
    }
    
    /*
     * Get the appid by Merchant CurrencyType and Merchant Type
     * 获得  mid 通过商户用的结算货币类型与商户类型
     * @param string $currencyType Merchant CurrencyType
     * @param string $merchantTYpe Merchant Type Online Offline or H5
     * @return string $appid Merchant APPID
     */
    public static function getAppIDByTwoTypes($currencyType, $merchantType) {
        if($currencyType == self::CAD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_IDS_TEST_CAD[$merchantType];
            else
                return self::$API_IDS_LIVE[$merchantType];
        }
        else if($currencyType == self::USD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_IDS_TEST_USD[$merchantType];
            else
                return self::$API_IDS_LIVE[$merchantType];
        }
        else if($currencyType == self::CNY_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_IDS_TEST_CNY[$merchantType];
            else
                return self::$API_IDS_LIVE[$merchantType];
        }
        return "";
    }

    /*
     * Get the appPassword by Merchant CurrencyType and Merchant Type
     * 获得  mid 通过商户用的结算货币类型与商户类型
     * @param string $currencyType Merchant CurrencyType
     * @param string $merchantTYpe Merchant Type Online Offline or H5
     * @return string $appPassword Merchant APP PASSWORD
     */
    public static function getAppPasswordByTwoTypes($currencyType, $merchantType) {
        if($currencyType == self::CAD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_PASSWORDS_TEST_CAD[$merchantType];
            else
                return self::$API_PASSWORDS_LIVE[$merchantType];
        }
        else if($currencyType == self::USD_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_PASSWORDS_TEST_USD[$merchantType];
            else
                return self::$API_PASSWORDS_LIVE[$merchantType];
        }
        else if($currencyType == self::CNY_MERCHANT) {
            if(self::$USING_LIVE_APISERVER == false)
                return self::$API_PASSWORDS_TEST_CNY[$merchantType];
            else
                return self::$API_PASSWORDS_LIVE[$merchantType];
        }
        return "";
    }
    
    /**
     * Get the merchant Currecy type
     * 获取商户的结算货币类型
     * @return string Merchant currency type
     */
    public static function getMerchantCurrencyType() {
        return self::$USING_MERCHANT_CURRECNY_TYPE;
    }
    
    /**
     * Get the API whole URL
     * 获取接口的完整网址
     * @param string $functionName
     * @return string API_URL
     */
    public static function getURL($functionName) {
        if(self::$USING_LIVE_APISERVER == false)
            return self::$API_HOST_URL_TEST . $functionName;
        else
            return self::$API_HOST_URL_LIVE . $functionName;
    }
    
    /**
     * Set the demo server URL
     * 设置服务器的 URL
     * @param string $value
     */
    public static function setDemoServerURL($value) {
        // $value = self::get_current_url();
        self::$DEMO_SERVER_URL = $value;
    }
    
    /**
     * Get the call back URL
     * 获取异步调用所使用的URL
     * @return string callback_URL
     */
    public static function getCallbackURL() {
        return self::$DEMO_SERVER_URL . self::CALLBACK_URL;
    }
    
    /**
     * Get H5 payment URL
     * 获取手机H5直接支付的网址
     * @return string WAP_URL
     */
    public static function getWapURL() {
        return self::$DEMO_SERVER_URL . self::WAP_URL;
    }

    /**
     * Get payment notification URL
     * 获取检查订单是否已支付的网址
     * @return string PAYMENT_NOTIFY_URL
     */
    public static function getPaymentNotifyURL() {
        return self::$DEMO_SERVER_URL . self::PAYMENT_NOTIFY_URL;
    }
    
    /**
     * Get payment complete URL
     * 获取支付成功后,商户跳转地址url
     * @return string PAYMENT_COMPLETE_URL
     */
    public static function getPaymentCompleteURL() {
        return self::$DEMO_SERVER_URL . self::PAYMENT_COMPLETE_URL;
    }
    
    /**
     * Get payment cancel URL
     * 获取支付取消时,商户跳转地址url
     * @return string PAYMENT_CANCEL_URL
     */
    public static function getPaymentCancelURL() {
        return self::$DEMO_SERVER_URL . self::PAYMENT_CANCEL_URL;
    }
    
    /**
     * Set log file folder
     * 设置日志文件目录
     * @param string $foldername
     */
    public static function setLogFolder($foldername) {
        ini_set('date.timezone', 'Asia/Shanghai');
        self::$LOG_FOLDER = $foldername;
    }
    
    /**
     * Get motionpay orderCSV file name
     * 获取motionpay orderCSV文件名
     * @return string filename with folder info
     */
    public static function getMotionPayOrderCSVFilename() {
        MotionPayConfig::setLogFolder(getcwd() . "/log/");
        $logFileName = "motionpayOrderCSV";
        if(self::$USING_LIVE_APISERVER == false) {
            $logFileName = "motionpayOrderCSVTest";
        }
        return self::$LOG_FOLDER . $logFileName . '.csv';
    }
    
    
    /**
     * Get motionpay paidOrderCSV file name
     * 获取motionpay paidOrderCSV_文件名
     * @return string filename with folder info
     */
    public static function getMotionPayPaidOrderCSVFilename() {
        MotionPayConfig::setLogFolder(getcwd() . "/log/");
        $logFileName = "motionpayPaidOrderCSV";
        if(self::$USING_LIVE_APISERVER == false) {
            $logFileName = "motionpayPaidOrderCSVTest";
        }
        return self::$LOG_FOLDER . $logFileName . '.csv';
    }
    
    /**
     * Get motionpay log file name
     * 获取motionpay日志文件名
     * @return string filename with folder info
     */
    public static function getMotionPayLogFilename() {
        MotionPayConfig::setLogFolder(getcwd() . "/log/");
        $logFileName = "motionpayLive";
        if(self::$USING_LIVE_APISERVER == false) {
            $logFileName = "motionpayTest";
        }
        return self::$LOG_FOLDER . $logFileName . date('Y-m-d') . '.log';
    }

    /**
     * Get motionpay callback file name
     * 获取motionpay异步通知日志文件名
     * @return string filename with folder info
     */
    public static function getMotionPayCallbackFilename() {
        MotionPayConfig::setLogFolder(getcwd() . "/log/");
        $logFileName = "motionpayCallbackLive";
        if(self::$USING_LIVE_APISERVER == false) {
            $logFileName = "motionpayCallbackTest";
        }
        return self::$LOG_FOLDER . $logFileName . '.log';
    }
    
    public static function getMid($merchantType) {
        return MotionPayConfig::getMidByTwoTypes(self::$USING_MERCHANT_CURRECNY_TYPE, $merchantType);
    }

    public static function getAppID($merchantType) {
        return MotionPayConfig::getAppIDByTwoTypes(self::$USING_MERCHANT_CURRECNY_TYPE, $merchantType);
    }
    
    public static function getAPI_PASSWORD($merchantType) {
        return MotionPayConfig::getAppPasswordByTwoTypes(self::$USING_MERCHANT_CURRECNY_TYPE, $merchantType);
    }
    
    /**
     * Put your business logic if the order has been paid.
     * @param string $out_trade_no orderNumber
     */
    /**
     * 订单被支付以后的业务逻辑
     * @param string $out_trade_no orderNumber
     */
    public static function logicForOrderPaid($out_trade_no) {
        // put your own code here.
        // 您可以在这里添加您的代码。
    }
    
    public static function get_current_url($strip = true) {
        static $filter, $scheme, $host;
        if (!$filter) {
            // sanitizer
            $filter = function($input) use($strip) {
                $input = trim($input);
                if ($input == '/') {
                    return $input;
                }
                
                // add more chars if needed
                $input = str_ireplace(["\0", '%00', "\x0a", '%0a', "\x1a", '%1a'], '',
                    rawurldecode($input));
                
                // remove markup stuff
                if ($strip) {
                    $input = strip_tags($input);
                }
                
                // or any encoding you use instead of utf-8
                $input = htmlspecialchars($input, ENT_QUOTES, 'utf-8');
                
                return $input;
            };
            
            $host = $_SERVER['SERVER_NAME'];
            $scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME']
            : ('http'. (($_SERVER['SERVER_PORT'] == '443') ? 's' : ''));
        }
        
        return sprintf('%s://%s%s', $scheme, $host, $filter($_SERVER['REQUEST_URI']));
    }
}


