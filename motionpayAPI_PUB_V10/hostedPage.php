

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>    
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
<script>
function imageSel(imageName) {
	var radioButtonEle = document.getElementById("bank_" + imageName);
	if(radioButtonEle) {
		radioButtonEle.checked = true;
	}
}
function validationForm(thisForm) {
	var radioButton1 = document.getElementById("bank_HOSTED_A");
	var radioButton2 = document.getElementById("bank_HOSTED_W");
	var radioButton3 = document.getElementById("bank_HOSTED_UNS");
	var radioButton4 = document.getElementById("bank_HOSTED_UNIFIED");

	
	if (radioButton1.checked == false && radioButton2.checked == false 
			&& radioButton3.checked == false && radioButton4.checked == false) {
		alert("Please select one payment method.");
	}
	else {
		document.getElementById("payment").submit();
	}	
}
var currencyNameSelected = "CAD";
function selectCurrency(currencyName) {
	var price = document.getElementById("paymentAmountInput").value;
	var priceLabel = currencyName + "$" + price;
	currencyNameSelected = currencyName;
	// alert(priceLabel);
	if(currencyName == 'CAD') {
		priceLabel = currencyName + "$" + price;
	}
	else if(currencyName == 'CNY') {
		if(parseFloat(price) < 0.1) {
			document.getElementById("paymentAmountInput").value = "0.10";
			price = document.getElementById("paymentAmountInput").value;
		}
		priceLabel = currencyName + "¥" + price;
	}
	else if(currencyName == 'USD') {
		priceLabel = currencyName + "$" + price;
	}
	document.getElementById("finalPriceLabel").innerHTML = priceLabel;
}
function updatePrice() {
	selectCurrency(currencyNameSelected);
}
</script>	
<style>
table, td, th {
    text-align: left;
}
</style>
  </head>
<body>

<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="http://motionpay.ca/" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
<div class="pay_infor"  >
    <div class="infor_box" style="height:30px;">
      <span >Sample Hosted Page by <font style="font-size:15px;font-weight:bold;color:#2489c4;">Motion Pay</font></span>     
      <span style="float:right;">
      <a href="index.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Index</font></a>
      <a href="orderList.php"><font style="font-size:15px;font-weight:bold;color:#f60;">Order List</font></a>
      </span> 
    </div>
</div>
  
  <form id="payment" action="payment.php" method="post" >
    <input type="hidden" id="currentURL" name="currentURL" value="" />
    <div class="bank_list accounts_pay" style="display:block;">
    
      <h6><label>Motion Pay Hosted Page Solution</label></h6>
      <div><img src='images/HostedPage.png'/ style="margin-left: 10px;"> </div><br/><br/>
      
      <table>
      <tr><td>merchant Type</td>
      <td><input type="radio" id="merchantTypeCAD" name="merchantType" value="CAD" checked />CAD merchant</td>
      <td><input type="radio" id="merchantTypeCNY" name="merchantType" value="CNY" />CNY merchant</td>
      <td><input type="radio" id="merchantTypeUSD" name="merchantType" value="USD" />USD merchant</td></tr>
      <tr><td>currency for Price:</td>
      <td><input type="radio" id="currencyTypeCAD" name="currencyType" value="CAD" checked
      			  		 onclick="JavaScript: selectCurrency('CAD');"/>CAD currency</td>
      <td><input type="radio" id="currencyTypeCNY" name="currencyType" value="CNY" 
      					 onclick="JavaScript: selectCurrency('CNY');"/>CNY currency</td>
      <td><input type="radio" id="currencyTypeUSD" name="currencyType" value="USD" 
      					 onclick="JavaScript: selectCurrency('USD');"/>USD currency</td></tr>
      </table>
      
      <table>
      	<tr>
      		<td><label>Final Price: <font class="cOrange" style="font-size: 25px;" id="finalPriceLabel">CAD$0.01</font></label></td>
     	  	<td><label>Type in amount: <input type="text" name="paymentAmount" value="0.01" size="5" id="paymentAmountInput"
 	  			onchange="JavaScript: updatePrice();"/></label> </td>
 	  	</tr>
 	  	<tr>
     	  	<td><label>SubMerName: <input type="text" name="subMerName" value="Waterloo University" size="15" id="subMerName"/>
     	  	</label></td> 
 	  	</tr>
 	  </table>
 	  	<br/><br/>
      
  	  <ul class="sel_list">
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_HOSTED_A" value="HOSTED_A" />
               <img src='images/alipayHosted.png'/>
             </label>
           </li>
         
      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_HOSTED_W" value="HOSTED_W" />
               <img src='images/wechatHosted.png'/>
             </label>
           </li>

      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_HOSTED_UNS" value="HOSTED_UNS" />
               <img src='images/unionpayHosted.png'/>
             </label>
           </li>   

      	   <li >
	      	<label>
          	   <input type="radio" name="paymentType" id="bank_HOSTED_UNIFIED" value="HOSTED_UNIFIED" />
               <img src='images/unifiedHosted.png'/>
             </label>
           </li>   
      </ul>
      <div class="confirm_pay">
        <a href="#" class="confirm_pay_btn" onclick="validationForm(this);"></a>
      </div>
    </div>
  </form>   
</div>

 

<script>
function getBaseURL() {
    var loc = window.location;
    var baseURL = loc.protocol + "//" + loc.hostname;
    if (typeof loc.port !== "undefined" && loc.port !== "") baseURL += ":" + loc.port;
    // strip leading /
    var pathname = loc.pathname;
    // pathname = pathname.replace("hostedPage.php","");
    var lastSlash = pathname.lastIndexOf("/");
    if(lastSlash > 0) {
    	pathname = pathname.substring(0,lastSlash+1);
    }
    if (pathname.length > 0 && pathname.substr(0,1) === "/") pathname = pathname.substr(1, pathname.length - 1);
    var pathParts = pathname.split("/");
    if (pathParts.length > 0) {
        for (var i = 0; i < pathParts.length; i++) {
            if (pathParts[i] !== "") baseURL += "/" + pathParts[i];
        }
    }
    // alert("url is:" + baseURL);
    return baseURL;
}
function setCurrentURL() {	
	var currentURL = document.getElementById("currentURL");
	if(currentURL != null) {
		// alert("not null!");
		currentURL.value = getBaseURL()  + "/";
		// alert(currentURL.value);
	}
}
setCurrentURL();
</script>	
</body>

</html>

